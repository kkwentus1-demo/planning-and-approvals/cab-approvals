## Change Approval Board Review Request


### Required from Submitter: 
- [ ] Set title to Issue: Project Name / Initiative
- [ ] Links to MR(s) below

   * 
- [ ] Set Milestone on this Issue as Desired Approval Release




### _Optional_: Additional Instructions or Context from Submitter
-  If Emergency/Priority request outside of standard 5 business days provide additional details below
-  If Emergency/Priority request outside of standard 5 business days add "Emergency CAB Request" label after submittal (this is not automated)




### Instructions for CAB Approvers
* Add a Comment below of your Approval status
* If you are the 2nd approver who is passing this request, update the label to "Change Advisory::Approved" and "Scheduled"
* Any rejection of this request, please add notes for justification in your comment, update the label to "Change Advisory::Rejected"

> Members of the CAB will automatically be notified of the Request once this Issue is Submitted

<!-------------------------------------------
Please Do Not Edit Below This Area
-------------------------------------------->

/label ~"Change Advisory::Pending Approval" 

This approval process is owned by @kkwentus1-demo/planning-and-approvals/change-advisory-board-approvers
