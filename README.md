# CAB Approvals

To Submit a Request

1) Open an Issue
2) Select Description Template = Change Approval Request
3) Follow instructions in the template


* CAB Approvers will automatically be notified of your request once the Issue is submitted

